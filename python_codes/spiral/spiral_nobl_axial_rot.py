import random
import matplotlib.pyplot as plt
import numpy as np
from numpy import sin, cos, pi
n=1000000
a=7
b=0.24
c=1
th=4*np.random.randn(n)
x=a*np.exp(b*th)*np.cos(th)
y=a*np.exp(b*th)*np.sin(th)
z=c
xx=a*np.exp(b*(th))*np.cos(th+2*np.pi/2)
yx=a*np.exp(b*(th))*np.sin(th+2*np.pi/2)
zx=c
sx=(np.random.exponential(a,n)/2)+(-np.random.exponential(a,n)/2)
sy=(np.random.exponential(a,n)/2)+(-np.random.exponential(a,n)/2)
sz=(np.random.exponential(a,n)/2)+(-np.random.exponential(a,n)/2)
xd = x+sx
yd = y+sy
zd = z+sz
xd1 = xx+sx
yd1 = yx+sy
zd1 = zx+sz
def rd(r1):
    return 5*np.exp(-1.*r1/20.) #Applying Exponential Profile to Disk
plt.figure(2,figsize=(9,6))
i=1
j=[]
ci1=[]
ccr=[]
for aa in [0]:
        # rotation aboutH Z axis
    ar=aa*pi/180.
    for bb in [0,30,45,60,75,90]:
        # rotation about Y axis
        br=bb*pi/180.
        for cc in [0]:
            # rotation about Z axis
            cr=cc*pi/180.
            xda1=xd*(cos(cr)*cos(br)*cos(ar)-sin(cr)*sin(ar))
            yda1=xd*(cos(cr)*cos(br)*sin(ar)+sin(cr)*cos(ar))
            zda1=xd*(-cos(cr)*sin(br))
            xb1=xd1*(cos(cr)*cos(br)*cos(ar)-sin(cr)*sin(ar))
            yb1=xd1*(cos(cr)*cos(br)*sin(ar)+sin(cr)*cos(ar))
            zb1=xd1*(-cos(cr)*sin(br))
            xd2=yd*(-sin(cr)*cos(br)*cos(ar)-cos(cr)*sin(ar))
            yd2=yd*(-sin(cr)*cos(br)*sin(ar)+cos(cr)*cos(ar))
            zd2=yd*(sin(cr)*sin(br))
            xb2=yd1*(-sin(cr)*cos(br)*cos(ar)-cos(cr)*sin(ar))
            yb2=yd1*(-sin(cr)*cos(br)*sin(ar)+cos(cr)*cos(ar))
            zb2=yd1*(sin(cr)*sin(br))
            xd3=zd*(sin(br)*cos(ar))
            yd3=zd*(sin(br)*sin(ar))
            zd3=zd*(cos(br))
            xb3=zd1*(sin(br)*cos(ar))
            yb3=zd1*(sin(br)*sin(ar))
            zb3=zd1*(cos(br))
            xdn = xda1+xd2+xd3
            ydn = yda1+yd2+yd3
            zdn = zda1+zd2+zd3
            xbn = xb1+xb2+xb3
            ybn = yb1+yb2+yb3
            zbn = zb1+zb2+zb3
            print (i)
            plt.subplot(2,3,i,aspect='equal')
            plt.tight_layout()
            plt.title(r"$\alpha$="+str(aa)+r"$\beta$="+str(bb)+r"$\gamma$="+str(cc))
            plt.xlim(-400,400)
            plt.ylim(-400,400)
            plt.plot(xdn,ydn,"b.",markersize=1)
            plt.plot(xbn,ybn,"b.",markersize=1)
            plt.grid(True)
            i=i+1
            j.append(r"$\alpha$="+str(aa)+r"$\beta$="+str(bb)+r"$\gamma$="+str(cc))
            for xo in range(0,96,4):
                rs1 = np.sqrt((xdn-xo)**2+ydn**2)	#taking projection of spiral1
                cs1=rs1[:]<=1
                rs2 = np.sqrt((xbn-xo)**2+ybn**2)	#taking projection of spiral2
                cs2=rs2[:]<=1
                drho1 = rd(rs1)[cs1]
                drho2 = rd(rs2)[cs2]
                i1 = np.sum(drho1)
                i2 = np.sum(drho2)
                ci1.append(i1+i2)
                ccr.append(xo)
ci1max=max(ci1)
print(ci1max)
ci2 = [x / ci1max for x in ci1]
ccrmax=max(ccr)
ccr1 = [c / ccrmax for c in ccr]
plt.figure(3,figsize=(12,8))
a=[];b=[]
k=0;l=0
for i in range(0,144,24):
    plt.xlabel('radial length')
    plt.ylabel('intensity')
    plt.plot(ccr[i:i+24],ci1[i:i+24],'o--',label=j[k])
    plt.legend()
    print(ccr[i:i+24],ci1[i:i+24])
    k=k+1  
plt.figure(4,figsize=(12,8)) 
for r in range(0,144,24):
    plt.xlabel('scaled radial length')
    plt.ylabel('scaled intensity')
    print(ccr1[r:r+24],ci2[r:r+24])
    plt.plot(ccr1[r:r+24],ci2[r:r+24],'o--',label=j[l])
    plt.legend()
    l=l+1
plt.show()
