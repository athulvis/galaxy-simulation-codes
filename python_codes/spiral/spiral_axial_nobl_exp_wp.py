import random
import matplotlib.pyplot as plt
import numpy as np
from numpy import sin, cos
n=1000000
a=7
b=0.24
c=1
th=4*np.random.randn(n)
x=a*np.exp(b*th)*np.cos(th)
y=a*np.exp(b*th)*np.sin(th)
z=c
xx=a*np.exp(b*(th))*np.cos(th+2*np.pi/2)
yx=a*np.exp(b*(th))*np.sin(th+2*np.pi/2)
zx=c
sx=(np.random.exponential(a,n)/2)+(-np.random.exponential(a,n)/2)
sy=(np.random.exponential(a,n)/2)+(-np.random.exponential(a,n)/2)
sz=(np.random.exponential(a,n)/2)+(-np.random.exponential(a,n)/2)
xd = x+sx; yd = y+sy; zd = z+sz
xd1 = xx+sx
yd1 = yx+sy
zd1 = zx+sz
def rd(r1):
    return 5*np.exp(-1.*r1/20.) #Applying Exponential Profile to Disk
j=231
cha1=[];ccha=[];cchb=[];i0hval=[]
for t in [0,30,45,60,75,90]:
    thr=t*np.pi/180.
    x1=xd
    y1=(yd*cos(thr))-(zd*sin(thr))
    z1=(zd*cos(thr))+(yd*sin(thr))
    x2=xd1
    y2=(yd1*cos(thr))-(zd1*sin(thr))
    z2=(zd1*cos(thr))+(yd1*sin(thr))
    plt.figure(1,figsize=(12,8))
    plt.subplot(j)
    plt.tight_layout() 
    plt.title(r'$\theta={0}$'.format(t))
    plt.xlim(-350,350)
    plt.ylim(-350,350)
    plt.grid(True)
    plt.plot(x1,y1,"b.",markersize=1)
    plt.plot(x2, y2,"b.",markersize=1)
    j =j+1
    rad1= np.sqrt((x1)**2+y1**2)
    ch1=rad1[:]<=1
    rad2= np.sqrt((x2)**2+y2**2)
    ch2=rad2[:]<=1
    hrho1=rd(rad1)[ch1]
    hrho2=rd(rad2)[ch2]
    i0h = np.sum(hrho1)+np.sum(hrho2)
    for xo4 in range(0,100,8):
        rh1 = np.sqrt((x1-xo4)**2+y1**2)	#taking projection of spiral1
        ch1=rh1[:]<=1
        rh2 = np.sqrt((x2+xo4)**2+y2**2)	#taking projection of spiral2
        ch2=rh2[:]<=1
        rhoh1=rd(rh1)[ch1]
        rhoh2=rd(rh2)[ch2]
        ih1 = np.sum(rhoh1)
        ih2 = np.sum(rhoh2)
        ccha.append(xo4)
        cha1.append((ih1+ih2))
        cchb.append(xo4/96.)
        i0hval.append(i0h)     
i0hmax=max(i0hval)
print(i0hmax)
cha2 = [x / i0hmax for x in cha1]
plt.figure(2,figsize=(12,8)) # intensity profile
plt.rcParams.update({'font.size': 14})
a=[];b=[]
labels=[r'$\theta={0}$',r'$\theta={30}$',r'$\theta={45}$',r'$\theta={60}$',r'$\theta={75}$',r'$\theta={90}$']
k=0
for i in range(0,78,13):
    plt.xlabel('axial length')
    plt.ylabel('intensity')
    #plt.plot(ccfa[0:12],cfa1[0:12],'ko--',label=r'$\theta={0}$' if k == 0 else "")
    plt.plot(ccha[i:i+13],cha1[i:i+13],'o--',label=labels[k])
    plt.legend()
    a.append(i)
    b.append(i+12)
    print(ccha[i:i+13],cha1[i:i+13])
    k=k+1
plt.figure(3,figsize=(12,8)) 
aa=[];bb=[]
kk=0
for u in range(0,78,13):
    plt.xlabel('scaled axial length')
    plt.ylabel('scaled intensity')
    #plt.plot(ccfb[0:12],cfa2[0:12],'ko',label=r'$\theta=0$'if kk == 0 else "")
    plt.plot(cchb[u:u+12],cha2[u:u+12],'o--',label=labels[kk])
    plt.legend()
    aa.append(u)
    bb.append(u+12)
    print(cchb[u:u+13],cha2[u:u+13])
    kk=kk+1
plt.show()
    
    
