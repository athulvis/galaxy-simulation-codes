import random
import matplotlib.pyplot as plt
import numpy as np
from numpy import sin, cos, pi
n=1000000
a=7
b=0.24
c=1
th=4*np.random.randn(n)
x=a*np.exp(b*th)*np.cos(th)
y=a*np.exp(b*th)*np.sin(th)
z=c
xx=a*np.exp(b*(th))*np.cos(th+2*np.pi/2)
yx=a*np.exp(b*(th))*np.sin(th+2*np.pi/2)
zx=c
sx=(np.random.exponential(a,n)/2)+(-np.random.exponential(a,n)/2)
sy=(np.random.exponential(a,n)/2)+(-np.random.exponential(a,n)/2)
sz=(np.random.exponential(a,n)/2)+(-np.random.exponential(a,n)/2)
xd = x+sx
yd = y+sy
zd = z+sz
xd1 = xx+sx
yd1 = yx+sy
zd1 = zx+sz
def rd(r1):
    return 5*np.exp(-1.*r1/20.) #Applying Exponential Profile to Disk
axial=[]
angle2 = []
cirval = []
j=[]
karr=[]
plt.figure(2,figsize=(9,6))
i=1
for aa in [0]:
    # rotation about Z axis
    ar=aa*pi/180.
    for bb in [0,30,45,60,75,90]:
        # rotation about Y axis
        br=bb*pi/180.
        for cc in [0]:
            # rotation about Z axis
            cr=cc*pi/180.
            xda1=xd*(cos(cr)*cos(br)*cos(ar)-sin(cr)*sin(ar))
            yda1=xd*(cos(cr)*cos(br)*sin(ar)+sin(cr)*cos(ar))
            zda1=xd*(-cos(cr)*sin(br))
            xb1=xd1*(cos(cr)*cos(br)*cos(ar)-sin(cr)*sin(ar))
            yb1=xd1*(cos(cr)*cos(br)*sin(ar)+sin(cr)*cos(ar))
            zb1=xd1*(-cos(cr)*sin(br))
            xd2=yd*(-sin(cr)*cos(br)*cos(ar)-cos(cr)*sin(ar))
            yd2=yd*(-sin(cr)*cos(br)*sin(ar)+cos(cr)*cos(ar))
            zd2=yd*(sin(cr)*sin(br))
            xb2=yd1*(-sin(cr)*cos(br)*cos(ar)-cos(cr)*sin(ar))
            yb2=yd1*(-sin(cr)*cos(br)*sin(ar)+cos(cr)*cos(ar))
            zb2=yd1*(sin(cr)*sin(br))
            xd3=zd*(sin(br)*cos(ar))
            yd3=zd*(sin(br)*sin(ar))
            zd3=zd*(cos(br))
            xb3=zd1*(sin(br)*cos(ar))
            yb3=zd1*(sin(br)*sin(ar))
            zb3=zd1*(cos(br))
            xdn = xda1+xd2+xd3
            ydn = yda1+yd2+yd3
            zdn = zda1+zd2+zd3
            xbn = xb1+xb2+xb3
            ybn = yb1+yb2+yb3
            zbn = zb1+zb2+zb3
            
            plt.subplot(2,3,i,aspect='equal')
            plt.grid(True,linestyle='--')
            plt.tight_layout()
            plt.title(r"$\alpha$="+str(aa)+r"$\beta$="+str(bb)+r"$\gamma$="+str(cc))
            plt.xlim(-400,400)
            plt.ylim(-400,400)
            plt.plot(xdn,ydn,'b.',markersize=1)
            plt.plot(xbn,ybn,'b.',markersize=1)
            print (i)
            i=i+1
            j.append(r"$\alpha$="+str(aa)+r"$\beta$="+str(bb)+r"$\gamma$="+str(cc))
            for k in range(2,70,6):
                rs1 = np.sqrt(xdn**2+ydn**2)	#taking projection of spiral1
                rs2 = np.sqrt(xbn**2+ybn**2)	#taking projection of spiral2
                #rddd = np.setxor1d(rs1,rs2)
                rd12 = rs1[:]<=k; rd21 = rs2[:]<=k
                rd14 = rs1[:]<=2*k; rd24 = rs2[:]<=2*k
                rdd1 = rd12 + rd21
                rdd2 = rd14 + rd24
                rdn1 = rdd2 & (~rdd1)
                drho1 = rd(rs1)[rdd1]
                drho2 = rd(rs2)[rdn1]
                id1 = np.sum(drho1)
                id2 = np.sum(drho2)
                cir2 = id1/id2
                karr.append(k)
                cirval.append(cir2)
                angle2.append(bb)
plt.figure(3,figsize=(12,8))
for i in(0,12,24,36,48,60):
    plt.xlabel(r'pitch angle  $\beta$')
    plt.ylabel('CIR')
    plt.plot(angle2[i],cirval[i],'ro',label='k=2')
from collections import OrderedDict
plt.figure(4,figsize=(12,8))
for i in(0,12,24,36,48,60):
    plt.xlabel(r'pitch angle  $\beta$')
    plt.ylabel('CIR')
    plt.plot(angle2[i],cirval[i],'ro',label='k=2')
    plt.plot(angle2[i+1],cirval[i+1],'go',label='k=10')
    plt.plot(angle2[i+2],cirval[i+2],'bo',label='k=18')
    plt.plot(angle2[i+3],cirval[i+3],'co',label='k=26')
    plt.plot(angle2[i+4],cirval[i+4],'mo',label='k=34')
    plt.plot(angle2[i+5],cirval[i+5],'yo',label='k=42')
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(),fontsize = 'large')
    print(angle2[i],cirval[i]) 
plt.show()
