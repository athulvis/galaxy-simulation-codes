import random
import matplotlib.pyplot as plt
import numpy as np
from numpy import sin, cos
n=1000000
a=7
b=0.24
c=1
th=4*np.random.randn(n)

x=a*np.exp(b*th)*np.cos(th)
y=a*np.exp(b*th)*np.sin(th)
z=c
xx=a*np.exp(b*(th))*np.cos(th+2*np.pi/2)
yx=a*np.exp(b*(th))*np.sin(th+2*np.pi/2)
zx=c
sx=(np.random.exponential(a,n)/2)+(-np.random.exponential(a,n)/2)
sy=(np.random.exponential(a,n)/2)+(-np.random.exponential(a,n)/2)
sz=(np.random.exponential(a,n)/2)+(-np.random.exponential(a,n)/2)
xd = x+sx; yd = y+sy; zd = z+sz
xd1 = xx+sx
yd1 = yx+sy
zd1 = zx+sz
def rd(r1):
    return 5*np.exp(-1.*r1/20.) #Applying Exponential Profile to Disk
cirarr1=[]
ccarr=[]
j=231
karr=[]
for t in [0,30,45,60,75,90]:
    thr=t*np.pi/180.
    x1=xd
    y1=(yd*cos(thr))-(zd*sin(thr))
    z1=(zd*cos(thr))+(yd*sin(thr))
    x2=xd1
    y2=(yd1*cos(thr))-(zd1*sin(thr))
    z2=(zd1*cos(thr))+(yd1*sin(thr))
    plt.figure(1,figsize=(18,12))
    plt.subplot(j)
    plt.tight_layout() 
    plt.title(t)
    plt.grid(True,linestyle='--')
    plt.xlim(-400,400)
    plt.ylim(-400,400)
    plt.plot(x1,y1,"b.",markersize=1)
    plt.plot(x2, y2,"b.",markersize=1)
    j =j+1 
    for k in range(2,70,4):
        rs1 = np.sqrt(x1**2+y1**2)	#taking projection of spiral1
        rs2 = np.sqrt(x2**2+y2**2)	#taking projection of spiral2
        #rddd = np.setxor1d(rs1,rs2)
        rd12 = rs1[:]<=k; rd21 = rs2[:]<=k
        rd14 = rs1[:]<=2*k; rd24 = rs2[:]<=2*k
        rdd1 = rd12 + rd21
        rdd2 = rd14 + rd24
        rdn1 = rdd2 & (~rdd1)
        drho1 = rd(rs1)[rdd1]
        drho2 = rd(rs2)[rdn1]
        id1 = np.sum(drho1)
        id2 = np.sum(drho2)
        cir2 = id1/id2
        karr.append(k)
        cirarr1.append(cir2)
        ccarr.append(t)
        #print(drho2)
        plt.figure(2, figsize=(12,8))
        plt.rcParams.update({'font.size': 14})
        plt.title("Variation of CIR with orientation")
        plt.xlabel('angle of inclination')
        plt.ylabel('Central Intensity Ratio')
        plt.xlim(-10,100)
        if k==2:
            plt.plot(t,cir2,'o', c='b',label= " R1 = 2" if t==0 else "")
        plt.legend(loc = 0, fontsize = 'small')
        plt.figure(3, figsize=(12,8))
        plt.rcParams.update({'font.size': 14})
        plt.title("Variation of CIR with orientation")
        plt.xlabel('Angle of inclination')
        plt.ylabel('Central Intensity Ratio')
        plt.xlim(-10,100)
        if k==2:
            plt.plot(t,cir2,'o', c='b',label= " R1 = 2" if t==0 else "")
        if k==6:
            plt.plot(t,cir2,'o', c='g',label=" R1 = 6 " if t==30 else "")
        if k==10:
            plt.plot(t,cir2,'o', c='r',label=" R1 = 10 " if t==45 else "")
        if k==14:
            plt.plot(t,cir2,'o',c='c',label=" R1 = 14 " if t==60 else "")
        if k==18:
            plt.plot(t,cir2,'o',c='m',label=" R1 = 18 " if t==75 else "")
        if k==22:
            plt.plot(t,cir2,'o',c='y',label=" R1 = 70 " if t==90 else "")
        plt.legend(loc = 0, fontsize = 'small')
plt.show()
