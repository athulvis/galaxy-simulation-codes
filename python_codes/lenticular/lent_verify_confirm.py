import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import tplquad,quad
from numpy import sin, cos
a= b=c=100; d=100.; n=50000000
box_vol1 = 8*a*b*c
rho1 = n/box_vol1
box_vol2 = d**3
rho2 = n/box_vol2
newarrd = []                     
errarrd = []
nnd = []
newarrb = []
errarrb = []
nnb = []
def cylvolumed(z, y, x):
    return 5*rho1*np.exp(-1*np.sqrt((x**2)+(y**2))/20.)
def cylvolumeb(k, l, m):
    return 1200*rho2*np.exp(-7.67*((np.sqrt(k**2+l**2+m**2)/3.)**0.25)) 
for xd0 in range(0,50,2):
    Volume101d,err2d = tplquad(cylvolumed, (xd0-1), (xd0+1),
    lambda x: (-1)*np.sqrt(1.-(x-xd0)**2),
    lambda x: np.sqrt(1.-(x-xd0)**2),
    lambda x,y: -20.*np.exp(-np.sqrt((x**2)+(y**2))/35.),
    lambda x,y: 20.*np.exp(-np.sqrt((x**2)+(y**2))/35.))
    newarrd.append(Volume101d)
    errarrd.append(err2d)
    nnd.append(xd0)
    if xd0 > 18: #for bulge whose radius <20
        continue
    Volume101b,err2b = tplquad(cylvolumeb, (xd0-1), (xd0+1),
    lambda k: -(1-((k-xd0)*(k-xd0)))**0.5,
    lambda k: (1-((k-xd0)*(k-xd0)))**0.5,                           
    lambda k,l: -1*(((20**2)-(k**2)-(l**2))**0.5), 
    lambda k,l: (((20**2)-(k**2)-(l**2))**0.5))
    
    newarrb.append(Volume101b)
    errarrb.append(err2b)
    nnb.append(xd0)
    
import numpy as np
from math import *
import matplotlib.pyplot as plt
from matplotlib import *
a=b=c=100.; r1=6.; r2=2.*r1; rbs=20; h=20;rds=35; n=50000000
x=np.random.uniform(-a,a,n)
y=np.random.uniform(-b,b,n)
z=np.random.uniform(-c,c,n)
r=(x*x+y*y+z*z)**0.5
b=r[:]<=rbs
rt=(x*x+y*y)**0.5
H=h*np.exp(-r/rds)
d=rt[:]<=a
c1=z[:]<=H
c2=z[:]>=-1*H
cd=(d & c1 & c2)
xdt=x[cd]; ydt=y[cd];zdt=z[cd]
xbt=x[b]; ybt=y[b]; zbt=z[b]
R1=((xdt*xdt)+(ydt*ydt)+(zdt*zdt))**0.5
R2=((xbt*xbt)+(ybt*ybt)+(zbt*zbt))**0.5
def rd(r1):
    return 5*np.exp(-1.*r1/20.) #Applying Exponential Profile to Disk
def rb(r2):
    return 12000*np.exp(-7.67*((r2/3.)**0.25)) #Applying de Vaculour's profile to Bulge
rdp=(xdt*xdt+ydt*ydt)**0.5
rbp=(xbt*xbt+ybt*ybt)**0.5
czd = rdp[:]<=1
rdoz = rd(R1)[czd]
izd0 = np.sum(rdoz)
czb = rbp[:]<=1
rboz = rb(R2)[czb]
izb0 = np.sum(rboz)
i0n = izd0+izb0  # intensity
cirarr=[]
ccarr=[]
for xo in range (0,50,2):
    c1 = ((xdt-xo)*(xdt-xo)+(ydt)**2)**0.5
    cg1 = c1[:]<=1
    rhod = rd(R1)[cg1]
    i1 = np.sum(rhod)
    c2 = ((xbt-xo)*(xbt-xo)+(ybt)**2)**0.5
    cg2 = c2[:]<=1
    rhob = rb(R2)[cg2]
    i2 = np.sum(rhob)
    cirarr.append(i1+i2)
    ccarr.append(xo)
intensity = np.pad(newarrb,(0,len(newarrd)-len(newarrb)),mode='constant')+newarrd #as the number of values from disk and bulge are different, we have to add 0s to end of smallest array, bulge, to make both arrays of same shape.
print(intensity)
imax=max(intensity)
print(imax)
intscale = [x / imax for x in intensity]
nndn = [x / max(nnd) for x in nnd]
centmax = max(cirarr)
print(centmax)
centscale = [x / centmax for x in cirarr]
plt.figure(1,figsize=(12,10))
plt.xlabel('scaled radial distance')
plt.ylabel('scaled intensity')
plt.plot(nndn,intscale,'ro',label='volume integration')
plt.plot(nndn,centscale,'bo',label='montecarlo method')
plt.legend(fontsize='large')
plt.show()
