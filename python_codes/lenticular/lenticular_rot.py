import numpy as np
import matplotlib.pyplot as plt
from numpy import sin, cos, pi

a=b=c=100
n = 100000000
x = np.random.uniform(-a,a,n)
y= np.random.uniform(-b,b,n)
z = np.random.uniform(-c,c,n)
r = np.sqrt(x**2+y**2+z**2)
rs=r[:]<=20
rdisc = np.sqrt(x**2+y**2)
rds = rdisc[:]<=a
H=20*np.exp(-r/35)
c1=z[:]<=H
c2=z[:]>=-1*H
rdz=(rds & c1 & c2)
xb = x[rs]; yb= y[rs]; zb=z[rs]
xd=x[rdz]; yd=y[rdz]; zd= z[rdz]
plt.figure(1)
plt.xlim(-110,110)
plt.ylim(-110,110)
plt.plot(xd, yd,'ro',markeredgecolor='k')
plt.plot(xb,yb, 'bo',markeredgecolor='k')
R1=((xd*xd)+(yd*yd)+(zd*zd))**0.5
R2=((xb*xb)+(yb*yb)+(zb*zb))**0.5
def rd(r1):
	return 5*np.exp(-1.*r1/20.)
def rb(r2):
	return 12000*np.exp(-7.67*((r2/3.)**0.25))
j=[];ccarr=[];cirarr=[]
i=1
for aa in [0]:
    # rotation about Z axis
    ar=aa*pi/180.
    for bb in [0,30,60,90,120,150]:
        # rotation about Y axis
        br=bb*pi/180.
        for cc in [0]:
            # rotation about Z axis
            cr=cc*pi/180.
   
            xb1=xb*(cos(cr)*cos(br)*cos(ar)-sin(cr)*sin(ar))
            yb1=xb*(cos(cr)*cos(br)*sin(ar)+sin(cr)*cos(ar))
            zb1=xb*(-cos(cr)*sin(br))
            xd1=xd*(cos(cr)*cos(br)*cos(ar)-sin(cr)*sin(ar))
            yd1=xd*(cos(cr)*cos(br)*sin(ar)+sin(cr)*cos(ar))
            zd1=xd*(-cos(cr)*sin(br))
            xb2=yb*(-sin(cr)*cos(br)*cos(ar)-cos(cr)*sin(ar))
            yb2=yb*(-sin(cr)*cos(br)*sin(ar)+cos(cr)*cos(ar))
            zb2=yb*(sin(cr)*sin(br))
            xd2=yd*(-sin(cr)*cos(br)*cos(ar)-cos(cr)*sin(ar))
            yd2=yd*(-sin(cr)*cos(br)*sin(ar)+cos(cr)*cos(ar))
            zd2=yd*(sin(cr)*sin(br))
            xb3=zb*(sin(br)*cos(ar))
            yb3=zb*(sin(br)*sin(ar))
            zb3=zb*(cos(br))
            xd3=zd*(sin(br)*cos(ar))
            yd3=zd*(sin(br)*sin(ar))
            zd3=zd*(cos(br))
            xbn = xb1+xb2+xb3
            ybn = yb1+yb2+yb3
            zbn = zb1+zb2+zb3
            xdn = xd1+xd2+xd3
            ydn = yd1+yd2+yd3
            zdn = zd1+zd2+zd3
            print (i)
            #plt.subplot(6,3,i,aspect='equal')
            #plt.tight_layout()
           # plt.title("a="+str(aa)+"b="+str(bb)+"c="+str(cc))
            #plt.xlim(-150,150)
            #plt.ylim(-150,150)
            #plt.plot(xdn,ydn,'ro')
            #plt.plot(xbn,ybn,'bo')
            #plt.grid(True)
            i=i+1
            j.append(r"$\alpha$="+str(aa)+r"$\beta$="+str(bb)+r"$\gamma$="+str(cc)) 
            for xo in range(0,96,4):
                rh1 = np.sqrt((xdn-xo)**2+ydn**2)
                rh2 = np.sqrt((xbn-xo)**2+ybn**2)
                rdn1=rh1[:]<=1
                drho1 = rd(R1)[rdn1]
                rbn1 = rh2[:]<=1
                brho1 = rb(R2)[rbn1]
                ib1 = np.sum(brho1)
                id1 = np.sum(drho1)
                i1 = ib1 + id1
                cirarr.append(i1)
                ccarr.append(xo)

print(ccarr,cirarr)
ci1max=max(cirarr)
print(ci1max)
cir = [x / ci1max for x in cirarr]
ccrmax=max(ccarr)
ccr = [c / ccrmax for c in ccarr]  

plt.figure(4,figsize=(12,8))
a=[];b=[]
k=0
l=0
for i in range(0,144,24):
    #plt.plot(ccr1[0:12],ci1[0:12],'o',label='a=0b=0cc=0'if k == 0 else "")
    plt.xlabel('radial length')
    plt.ylabel('intensity')
    plt.plot(ccarr[i:i+24],cirarr[i:i+24],'o--',label=j[k])
    plt.legend()
    print(ccarr[i:i+24],cirarr[i:i+24])
    print(k)
    k=k+1
plt.figure(5,figsize=(12,8))
for r in range(0,144,24):
    plt.xlabel('scaled radial length')
    plt.ylabel('scaled intensity')
    print(ccr[r:r+24],cir[r:r+24])
    plt.plot(ccr[r:r+24],cir[r:r+24],'o--',label=j[l])
    plt.legend()
    l=l+1
plt.show()
