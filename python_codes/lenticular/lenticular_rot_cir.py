import numpy as np
import matplotlib.pyplot as plt
from numpy import sin, cos, pi
a=b=c=100
n = 100000000
x = np.random.uniform(-a,a,n)
y= np.random.uniform(-b,b,n)
z = np.random.uniform(-c,c,n)
r = np.sqrt(x**2+y**2+z**2)
rs=r[:]<=20
rdisc = np.sqrt(x**2+y**2)
rds = rdisc[:]<=a
H=20*np.exp(-r/35)
c1=z[:]<=H
c2=z[:]>=-1*H
rdz=(rds & c1 & c2)
xb = x[rs]; yb= y[rs]; zb=z[rs]
xd=x[rdz]; yd=y[rdz]; zd= z[rdz]
plt.figure(1)
plt.xlim(-110,110)
plt.ylim(-110,110)
plt.plot(xd, yd,'ro',markeredgecolor='k')
plt.plot(xb,yb, 'bo',markeredgecolor='k')
R1=((xd*xd)+(yd*yd)+(zd*zd))**0.5
R2=((xb*xb)+(yb*yb)+(zb*zb))**0.5
def rd(r1):
	return 5*np.exp(-1.*r1/20.)
def rb(r2):
	return 12000*np.exp(-7.67*((r2/3.)**0.25))
axial=[]
angle1 = []
cirval = []
j=[]
karr=[]
plt.figure(2,figsize=(9,6))
i=0
for aa in [0]:
    # rotation about Z axis
    ar=aa*pi/180.
    for bb in [0,30,45,60,75,90]:
        # rotation about Y axis
        br=bb*pi/180.
        for cc in [0]:
            # rotation about Z axis
            cr=cc*pi/180.
            
            xb1=xb*(cos(cr)*cos(br)*cos(ar)-sin(cr)*sin(ar))
            yb1=xb*(cos(cr)*cos(br)*sin(ar)+sin(cr)*cos(ar))
            zb1=xb*(-cos(cr)*sin(br))
            xd1=xd*(cos(cr)*cos(br)*cos(ar)-sin(cr)*sin(ar))
            yd1=xd*(cos(cr)*cos(br)*sin(ar)+sin(cr)*cos(ar))
            zd1=xd*(-cos(cr)*sin(br))
            xb2=yb*(-sin(cr)*cos(br)*cos(ar)-cos(cr)*sin(ar))
            yb2=yb*(-sin(cr)*cos(br)*sin(ar)+cos(cr)*cos(ar))
            zb2=yb*(sin(cr)*sin(br))
            xd2=yd*(-sin(cr)*cos(br)*cos(ar)-cos(cr)*sin(ar))
            yd2=yd*(-sin(cr)*cos(br)*sin(ar)+cos(cr)*cos(ar))
            zd2=yd*(sin(cr)*sin(br))
            xb3=zb*(sin(br)*cos(ar))
            yb3=zb*(sin(br)*sin(ar))
            zb3=zb*(cos(br))
            xd3=zd*(sin(br)*cos(ar))
            yd3=zd*(sin(br)*sin(ar))
            zd3=zd*(cos(br))
            xbn = xb1+xb2+xb3
            ybn = yb1+yb2+yb3
            zbn = zb1+zb2+zb3
            xdn = xd1+xd2+xd3
            ydn = yd1+yd2+yd3
            zdn = zd1+zd2+zd3
            rdn = np.sqrt(xdn**2+ydn**2)
            rbn = np.sqrt(xbn**2+ybn**2)
            print(i)
            i=i+1
            plt.grid(True,linestyle='--')
            plt.subplot(2,3,i,aspect='equal')
            plt.tight_layout()
            plt.title(r'$\alpha$='+str(aa)+r' $\beta$='+str(bb)+r' $\gamma$='+str(cc))
            plt.xlim(-150,150)
            plt.ylim(-150,150)
            plt.plot(xdn,ydn,'ro',markeredgecolor='k')
            plt.plot(xbn,ybn,'bo',markeredgecolor='k')
            j.append("a="+str(aa)+"b="+str(bb)+"c="+str(cc)) 
            for k in range(2,50,8):
                rdn1 = np.where(rdn<=k) #inner disk radius
                rdn2 = np.where(rdn<=2*k) #outer disk radius
                rdnn = np.setxor1d(rdn2,rdn1)
                rbn1 = np.where(rbn<=k) #inner bulge radius
                rbn2 = np.where(rbn<=2*k) #outer bulge radius
                rbnn = np.setxor1d(rbn2,rbn1)
                drho1 = rd(R1)[rdn1]
                drho2 = rd(R1)[rdnn]
                brho1 = rb(R2)[rbn1]
                brho2 = rb(R2)[rbnn]
                ib1 = np.sum(brho1)
                id1 = np.sum(drho1)
                i1 = ib1 + id1
                ib2 = np.sum(brho2)
                id2 = np.sum(drho2)
                i2 = ib2 + id2
                cir = i1/i2
                cirval.append(cir)
                angle1.append(bb)
                karr.append(k)
from collections import OrderedDict
plt.figure(3,figsize=(12,8))
for i in(0,6,12,18,24,30):
    plt.xlabel(r'pitch angle, $\beta$')
    plt.ylabel('CIR')
    plt.plot(angle1[i],cirval[i],'ro',label='k=2')
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(),fontsize = 'large')
plt.figure(4,figsize=(12,8))
for i in(0,6,12,18,24,30):
    plt.xlabel(r'pitch angle, $\beta$')
    plt.ylabel('CIR')
    plt.plot(angle1[i],cirval[i],'ro',label='k=2')
    plt.plot(angle1[i+1],cirval[i+1],'go',label='k=10')
    plt.plot(angle1[i+2],cirval[i+2],'bo',label='k=18')
    plt.plot(angle1[i+3],cirval[i+3],'co',label='k=26')
    plt.plot(angle1[i+4],cirval[i+4],'mo',label='k=34')
    plt.plot(angle1[i+5],cirval[i+5],'yo',label='k=42')
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(),fontsize = 'large')
    print(angle1[i],cirval[i])
plt.show()
