import numpy as np
import matplotlib.pyplot as plt
from numpy import sin, cos
#creating disk and sphere with given parameters
a=b=c=100
n = 10000000
x = np.random.uniform(-a,a,n)
y= np.random.uniform(-b,b,n)
z = np.random.uniform(-c,c,n)
r = np.sqrt(x**2+y**2+z**2)
rs=r[:]<=20 #radius of bulge set to 20
rdisc = np.sqrt(x**2+y**2)
rds = rdisc[:]<=a #radius of disk set to 100
H=20.*np.exp(-r/35.) #Exponential shape to disk
c1=z[:]<=H
c2=z[:]>=-1*H
rdz=(rds & c1 & c2) 
xb = x[rs]; yb= y[rs]; zb=z[rs]
xd=x[rdz]; yd=y[rdz]; zd= z[rdz]
R1=((xd*xd)+(yd*yd)+(zd*zd))**0.5
R2=((xb*xb)+(yb*yb)+(zb*zb))**0.5
def rd(r1):
	return 5*np.exp(-1.*r1/20.) #Applying Exponential Profile to Disk
def rb(r2):
	return 12000*np.exp(-7.67*((r2/3.)**0.25)) #Applying de Vaculour's profile to Bulge
cirarr=[]
ccarr=[]
karr=[]
j=231
#Rotating disk and bulge along X axis
for t in (0,30,45,60,75,90):
	thr= t*np.pi/180.
	x1=xd
	y1=(yd*cos(thr))-(zd*sin(thr))
	z1=(zd*cos(thr))+(yd*sin(thr))
	x2=xb
	y2=(yb*cos(thr))-(zb*sin(thr))
	z2=(zb*cos(thr))+(yb*sin(thr))
	plt.subplot(j)
	plt.grid(True,linestyle='--')
    plt.tight_layout() 
	plt.title(t)
	plt.xlim(-120,120)
	plt.ylim(-120,120)
	plt.plot(x1,y1,'ro',markeredgecolor='k')
	plt.plot(x2,y2,'bo',markeredgecolor='k')
	j =j+1
	#plt.savefig("galsubplot.png")
	rdn = np.sqrt(x1**2+y1**2)	#taking projection of disk
	rbn = np.sqrt(x2**2+y2**2)	#taking projection of bulge
	for k in range(2,38,4):
		rdn1 = np.where(rdn<=k)	#inner disk radius
		rdn2 = np.where(rdn<=2*k)	#outer disk radius
		rdnn = np.setxor1d(rdn2,rdn1)
		rbn1 = np.where(rbn<=k) #inner bulge radius
		rbn2 = np.where(rbn<=2*k) #outer bulge radius
		rbnn = np.setxor1d(rbn2,rbn1) #storing vlues excluding coomon values in rbn1 and rbn2
		drho1 = rd(R1)[rdn1] 
		drho2 = rd(R1)[rdnn]
		brho1 = rb(R2)[rbn1]
		brho2 = rb(R2)[rbnn]
		ib1 = np.sum(brho1)
		id1 = np.sum(drho1)
		i1 = ib1 + id1	#intensity of first shell including disk and bulge
		ib2 = np.sum(brho2)
		id2 = np.sum(drho2)
		i2 = ib2 + id2	#intensity of second shell including disk and bulge	
		cir = i1/i2	#Central Intensity Ratio
		cirarr.append(cir)
		ccarr.append(t)
		karr.append(k)
		plt.figure(2)
		plt.xlim(-10,100)
		plt.title("Variation of CIR with orientation")
		plt.xlabel('angle of inclination')
		plt.ylabel('Central Intensity Ratio')
		if k==2:
			plt.plot(t,cir,'r.',label= " R1 = 2" if t==0 else "")
		if k==6:
			plt.plot(t,cir,'g.',label=" R1 = 6 " if t==30 else "")
		if k==10:
			plt.plot(t,cir,'b.',label=" R1 = 10 " if t==45 else "")
		if k==14:
			plt.plot(t,cir,'c.',label=" R1 = 14 " if t==60 else "")
		if k==18:
			plt.plot(t,cir,'m.',label=" R1 = 18 " if t==75 else "")
		if k==22:
			plt.plot(t,cir,'k.',label=" R1 = 22 " if t==90 else "")
		
		plt.legend(loc = 0, fontsize = 'small')
plt.show()
			

