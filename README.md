# Galaxy Simulation Codes

The python codes created for my galaxy simulation project. The codes are written to study the orientation effects of spiral and lenticular galaxies on their intensity profiles and CIR.
